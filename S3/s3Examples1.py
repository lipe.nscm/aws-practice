import boto3

# Com as credenciais da AWS devidamente configuradas na máquina, instanciamos o boto3.client
s3 = boto3.client('s3')
# Vamos criar um bucket
bucket = s3.create_bucket(Bucket='belgium-test-staging')
bucket = s3.create_bucket(Bucket='belgium-test-processed')
bucket = s3.create_bucket(Bucket='belgium-test-logs')


def listar_buckets():
    print('buckets existentes :')
    for response in s3.list_buckets()['Buckets']:
        print(response['Name'])


#listando buckets e printando no console
listar_buckets()

# deletando os buckets
for r in s3.list_buckets()['Buckets']:
    print(r['Name'])
    response = s3.delete_bucket(Bucket=r['Name'])
#OBS:Caso algum de seus buckets possuam algum objeto dentro, ele irá disparar uma exceção e não prosseguirá deletando os buckets criados

listar_buckets()
