import boto3

s3 = boto3.client('s3')

bucket = s3.create_bucket(Bucket='belgium-test-staging')


response = s3.upload_file(
    Bucket='belgium-test-staging',
    Filename='./dados/discentes-ingressos-graduacao-2019-ufpe.csv',
    Key='2019/discentes-ingressos.csv' )


response = s3.list_objects(
    Bucket='belgium-test-staging', Prefix='2019/discentes-')


for obj in response['Contents']:
    s3.put_object_acl(Bucket='belgium-test-staging', Key=obj['Key'], ACL='private')
    # print('https://{}.s3.amazonaws.com/{}'.format('belgium-test-staging', obj['Key']))

#ao final descomente e rode o script com os códigos abaixo para remover o arquivo e o bucket.
# s3.delete_object(Bucket='belgium-test-staging', Key='2019/discentes-ingressos.csv')
# s3.delete_bucket(Bucket='belgium-test-staging')
