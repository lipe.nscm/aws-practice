**Projeto de estudo para gestão de recursos na AWS.**

**- s3Examples1.py :**
    _- Instanciando um client usando a biblioteca boto3;
    - Criando buckets;
    - Listando buckets criados;
    - Deletando buckets criados;_

**- s3Examples2.py :**
_    - Realizando upload de um objeto para o bucket;
_    - Lendo os objetos e seus metadados de um bucket;
  _  - Baixando um objeto de um bucket;
_    - Removendo um objeto;____
