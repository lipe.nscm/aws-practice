import pandas as pd
import boto3

s3 = boto3.client('s3')

#Lendo através de leitura direta através da conexãoo client do boto
# obj = s3.get_object(Bucket='belgium-test-staging', Key='2019/discentes-ingressos.csv')
# df = pd.read_csv(obj['Body'])
# print(df.head)
# df = pd.read_csv('https://belgium-test-staging.s3.amazonaws.com/2019/discentes-ingressos.csv')

#gerando uma presigned url

url_share = s3.generate_presigned_url(
    ClientMethod='get_object' , 
    ExpiresIn='600', 
    Params={
        'Bucket':'belgium-test-staging',
        'Key':'2019/discentes-ingressos.csv'
    }
)

df = pd.read_csv(url_share)
#URL Gerada
print(url_share)
#Conteúdo
print(df.shape)


#ao final descomente e rode o script com os códigos abaixo para remover o arquivo e o bucket.
s3.delete_object(Bucket='belgium-test-staging', Key='2019/discentes-ingressos.csv')
s3.delete_bucket(Bucket='belgium-test-staging')

