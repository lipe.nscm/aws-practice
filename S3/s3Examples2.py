import boto3


s3 = boto3.client('s3', region_name='us-east-1')

# Vamos criar um bucket
bucket = s3.create_bucket(Bucket='belgium-test-staging')

# Em seguida vamos realizar o upload de um arquivo para nosso bucket
response = s3.upload_file(
    Bucket='belgium-test-staging',
    Filename='./dados/discentes-ingressos-graduacao-2018-ufpe.csv',
    Key='discentes-ingressos-graduacao-2018-ufpe.csv' )

# Listando objetos em um bucket
response = s3.list_objects(Bucket='belgium-test-staging', MaxKeys = 1)
print(response)

# Exibindo metadados dos objetos em um bucket

response = s3.head_object(Bucket='belgium-test-staging', Key='discentes-ingressos-graduacao-2018-ufpe.csv')
print(response)
print('o tamanho do arquivo {} é de {} bytes'.format(response['ETag'], response['ContentLength']))


# Baixando o arquivo do bucket
response = s3.download_file(Bucket='belgium-test-staging', Key='discentes-ingressos-graduacao-2018-ufpe.csv', Filename='discentes-2018-ufpe-download.csv')

# Deletando o arquivo do bucket
response = s3.delete_object(Bucket='belgium-test-staging', Key='discentes-ingressos-graduacao-2018-ufpe.csv')

#Deletando o bucket
response = s3.delete_bucket(Bucket='belgium-test-staging')
