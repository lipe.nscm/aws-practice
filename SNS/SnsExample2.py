import boto3

#Criando tópicos a partir de uma lista
#Acrescentando um prefixo "bloco_"
#removendo os tópicos que não possuem a palavra bloco do nome
def main():

    sns = boto3.client('sns')
    lista = ['bolo_de_rolo','tapioca','cocada']
    for item in lista:
        print(sns.create_topic(Name="bloco_{}".format(item))['TopicArn'])

    print("Listando todos tópicos criados:")
    topics = sns.list_topics()['Topics']
    print(topics)

    for i in topics:
        if "bloco" not in i['TopicArn']:
            print("Removendo " + i['TopicArn'])
            sns.delete_topic(TopicArn=i['TopicArn'])



if __name__=='__main__':
    main()