import pandas as pd 
import boto3

# Lendo um arquivo do bucket contendo telefones 
# e emails por departamento e subscrevendo-os em 
# um tópico.

CRIAR_BUCKET = True
BUCKET_NAME = 'bt-lakeraichu-dev'
KEY_NAME = 'contatos.csv'
def main():
    print("Iniciando ...")
    url_arquivo_contatos = ""
    sns = boto3.client('sns')

    if(CRIAR_BUCKET):
        s3 = boto3.client('s3')
        s3.create_bucket(Bucket=BUCKET_NAME)
        s3.upload_file(
            Bucket=BUCKET_NAME,
            Filename='./contatos.csv',
            Key=KEY_NAME )
        s3.put_object_acl(Bucket=BUCKET_NAME, Key='contatos.csv', ACL='public-read')
        url_arquivo_contatos = 'https://{}.s3.amazonaws.com/{}'.format(BUCKET_NAME, KEY_NAME)
    print(url_arquivo_contatos)
    contatos = pd.read_csv(url_arquivo_contatos)

    tapioca_arn = sns.create_topic(Name='tp-tapioca')['TopicArn']
    cocada_arn = sns.create_topic(Name='tp-cocada')['TopicArn']

    def subscribe(contato_row):
        if contato_row['topico'] == "tapioca":
            sns.subscribe(TopicArn=tapioca_arn, Protocol='sms', Endpoint=str(contato_row['telefone']))
            sns.subscribe(TopicArn=tapioca_arn, Protocol='email', Endpoint=str(contato_row['email']))
        else:
            sns.subscribe(TopicArn=cocada_arn, Protocol='sms', Endpoint=str(contato_row['telefone']))
            sns.subscribe(TopicArn=cocada_arn, Protocol='email', Endpoint=str(contato_row['email']))


    contatos.apply(subscribe, axis=1)


    


if __name__=='__main__':
    main()