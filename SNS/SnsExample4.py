import boto3


#No arquivo anterior criamos os subscribers
#agora vamos listá-los pelo ARN do tópico
#removendo uma subscrição pelo ARN da subscrição
def main():
    sns = boto3.client('sns')
    topic_target = ""
    topics = sns.list_topics()['Topics']
    for topic in topics:
        if "bolo_de_rolo" in topic['TopicArn']:
            topic_target = topic['TopicArn']
            print("Subscrição no tópico : " + topic_target)

    response = sns.list_subscriptions_by_topic(
        TopicArn=topic_target
    )
    print(response['Subscriptions'])
    # sns.unsubscribe(
    #     SubscriptionArn=""
    # )

if __name__=='__main__':
    main()