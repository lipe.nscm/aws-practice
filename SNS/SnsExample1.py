import boto3

# Criando um tópico no SNS
# 1º Instanciando o client do SNS pelo boto3
# 2º Criando um tópico e armazenando o retorno
# 3º Exibindo o ARN gerado para o tópico criado
# 4º Exibindo os tópicos criados
# 5º Removendo o tópico recém criado

def main():
    print("iniciando...")
    sns = boto3.client('sns')
    response = sns.create_topic(Name='bolo_de_rolo')
    arn_topic = response['TopicArn']
    print(arn_topic)
    topics = sns.list_topics()
    print(topics)
    for i in topics['Topics']:
        print("Removendo "+i['TopicArn'])
        #Se quiser remover mesmo só descomentar a linha abaixo blz
        # sns.delete_topic(TopicArn=topics[i]['TopicArn'])

    # Forma abreviada de criar o tópico
    # arn_topic = sns.create_topic('bolo_de_rolo')['TopicArn']
    # Informação importante:
    # Caso venhamos a tentar outro tópico como o mesmo nome
    # ele irá retornar a ARN do tópico já criado

if __name__=='__main__':
    main()