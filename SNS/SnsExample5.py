import boto3


#No arquivo anterior criamos os subscribers
#agora vamos listá-los pelo ARN do tópico
#removendo uma subscrição pelo ARN da subscrição
def main():
    sns = boto3.client('sns')
    topic_target = ""
    topics = sns.list_topics()['Topics']
    for topic in topics:
        if "bolo_de_rolo" in topic['TopicArn']:
            topic_target = topic['TopicArn']
            print("Subscrição no tópico : " + topic_target)

    response = sns.publish(
        TopicArn=topic_target,
        Message="This is a swing test!",
        Subject="Assunto da semana"
    )
    print(response)

    response = sns.publish(
        PhoneNumber="+number_telefone",
        Message="chã,patinho,lagarto"
    )
    print(response)
if __name__=='__main__':
    main()