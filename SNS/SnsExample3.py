import boto3


#Criando uma subscrição por SMS
#Criando uma subscrição por EMAIL

def main():
    sns = boto3.client('sns')
    topic_target = ""
    topics = sns.list_topics()['Topics']
    for topic in topics:
        if "bolo_de_rolo" in topic['TopicArn']:
            topic_target = topic['TopicArn']
            print("Subscrição no tópico : "+topic_target)

    response = sns.subscribe(
        TopicArn = topic_target,
        Protocol = 'SMS',
        Endpoint = '+numero de telefone valido'
    )
    print(response['SubscriptionArn'])
    response = sns.subscribe(
        TopicArn=topic_target,
        Protocol='email',
        Endpoint='lipe.nscm@gmail.com'
    )
    print(response['SubscriptionArn'])
    #Neste caso o e-mail receberá um alerta pedindo para confirma a subscrição via e-mail

if __name__=='__main__':
    main()